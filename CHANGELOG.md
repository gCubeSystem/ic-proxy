# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.4.0] - [2023-03-09]

- Feature #24254 simple serviceEndpoint creation 
- add support for UMA token
- Feature #24253 add support for decrypted ServiceEndpoint
- update lombok library to 1.18.4 with scope provided


## [v1.2.0] - [2021-06-08]

- Feature #21584 added support for /ServiceEndpoint/{category} REST call


## [v1.1.0-SNAPSHOT] - [2016-10-03]

- porting to auth v.2


## [v1.0.0] - [2015-07-01]

- First commit