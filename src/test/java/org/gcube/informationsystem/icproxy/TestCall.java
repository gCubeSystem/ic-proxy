package org.gcube.informationsystem.icproxy;

import javax.ws.rs.core.Application;

//import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.informationsystem.icproxy.resources.GCoreEndpointResource;
import org.gcube.informationsystem.icproxy.resources.GenericResourceResource;
import org.gcube.informationsystem.icproxy.resources.HostingNodeResource;
import org.gcube.informationsystem.icproxy.resources.ICResource;
import org.gcube.informationsystem.icproxy.resources.ServiceEndpointResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

public class TestCall extends JerseyTest{

	@Override
	protected Application configure() {
		ScopeProvider.instance.set("/gcube/devsec");
		return new ResourceConfig(ICResource.class,GCoreEndpointResource.class, ServiceEndpointResource.class, HostingNodeResource.class, GenericResourceResource.class);

	}

	@Test
	public void gcoreEndpoint() {
		final String ret = target("GCoreEndpoint").path("DataAnalysis").queryParam("scope", "/gcube/devsec").request().get(String.class);
		System.out.println("return is "+ret);
	}

	@Test
	public void gcoreEndpointWithServicename() {
		final String ret = target("GCoreEndpoint").path("DataAnalysis").path("RConnector")
				.queryParam("scope", "/gcube/devsec")
				.request().get(String.class);
		System.out.println(ret);
	}
	
	@Test
	public void gcoreEndpointWithResult() {
		final String ret = target("GCoreEndpoint").path("DataAnalysis")
				.queryParam("result","/Profile/AccessPoint/RunningInstanceInterfaces//Endpoint[@EntryName/string() eq \"querymanager\"]")
				.queryParam("scope", "/gcube/devsec").request().get(String.class);
		System.out.println(ret);
	}

	@Test
	public void serviceEndpoint() {
		final String ret = target("ServiceEndpoint").path("BiodiversityRepository").path("CatalogueOfLife").queryParam("scope", "/gcube/devsec").request().get(String.class);
		System.out.println(ret);
	}

	@Test
	public void serviceEndpointFree() {
		final String ret = target("ServiceEndpoint").path("Storage").path("StorageManager").queryParam("decrypt", true).queryParam("scope", "/gcube/devsec").request().get(String.class);
		System.out.println(ret);
	}
	
	@Test
	public void hostingNode() {
		final String ret = target("HostingNode").queryParam("scope", "/gcube/devsec").request().get(String.class);
		System.out.println(ret);
	}
	
	@Test
	public void gCoreEnpointsForHostingNode() {
		final String ret = target("HostingNode").path("92ee1020-5604-11e3-8182-e7053f61b8fe").path("GCoreEnpoints").queryParam("scope", "/gcube/devsec").request().get(String.class);
		System.out.println(ret);
	}
	
	@Test
	public void getById() {
		final String ret = target("/").path("aab08cf4-ed27-406c-b4a2-89888300976f").queryParam("scope", "/gcube/devsec").request().get(String.class);
		System.out.println(ret);
	}
	
}
