package org.gcube.informationsystem.icproxy;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/gcube/service/")
public class ICProxy extends ResourceConfig {

	public ICProxy() {
		packages("org.gcube.informationsystem.icproxy.resources");
	}

}
