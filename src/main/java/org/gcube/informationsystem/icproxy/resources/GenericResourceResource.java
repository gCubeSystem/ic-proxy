package org.gcube.informationsystem.icproxy.resources;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lombok.extern.slf4j.Slf4j;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;

@Path("GenericResource")
@Slf4j
public class GenericResourceResource {

	
	@GET
	@Path("/{secondaryType}/{name}")
	@Produces(MediaType.APPLICATION_XML)
	public List<GenericResource> retrieveByTypeAndName(@NotNull @PathParam("secondaryType") String secondaryType, 
			@NotNull @PathParam("name") String resourceName) {
		
		String scope = ScopeProvider.instance.get();
		log.info("genericResource called with secondaryType {} and name {} in scope {}",secondaryType, resourceName, scope);
		SimpleQuery query = createSecondaryTypeQuery(secondaryType)
				.addCondition(String.format("$resource/Profile/Name/text() eq '%s'",resourceName));
		DiscoveryClient<GenericResource> client = clientFor(GenericResource.class);
		List<GenericResource> endpoints = client.submit(query);
		log.debug("retrieved resources are "+endpoints.size());
		return endpoints; 
	}
	
	@GET
	@Path("/{secondaryType}")
	@Produces(MediaType.APPLICATION_XML)
	public List<GenericResource> retrieveByType(@NotNull @PathParam("secondaryType") String secondaryType, 
			@NotNull @PathParam("name") String resourceName) {
		String scope = ScopeProvider.instance.get();
		log.info("genericResource called with secondaryType {} in scope {}",secondaryType, scope);
		SimpleQuery query = createSecondaryTypeQuery(secondaryType);
		DiscoveryClient<GenericResource> client = clientFor(GenericResource.class);
		List<GenericResource> endpoints = client.submit(query);
		log.debug("retrieved resources are "+endpoints.size());
		return endpoints; 
	}
	
	SimpleQuery createSecondaryTypeQuery(String secondaryTpe){
		SimpleQuery query = queryFor(GenericResource.class);
		query.addCondition(String.format("$resource/Profile/SecondaryType/text() eq '%s'",secondaryTpe));
		return query;
	}
	
}
