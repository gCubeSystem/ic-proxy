package org.gcube.informationsystem.icproxy.resources;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;

import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.Query;
import org.gcube.resources.discovery.client.queries.impl.QueryBox;
import org.gcube.resources.discovery.icclient.ICFactory;

@Path("/")
@Slf4j
public class ICResource {

	@GET
	@Path("/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getById(@NotNull @PathParam("id") String id){
		log.info("resource request with id ",id);
						
		Query q = new QueryBox(String.format("declare namespace ic = 'http://gcube-system.org/namespaces/informationsystem/registry'; " +
				"for $resource in collection('/db/Profiles')//Document/Data/ic:Profile/Resource " +
				"where ($resource/ID/text() eq '%s') return $resource", id));
		DiscoveryClient<String> client = ICFactory.client();
		List<String> resources = client.submit(q);
		if (resources.size()>0)
			return resources.get(0);
		else throw new WebApplicationException("id "+id+" not found",Response.Status.NOT_FOUND);
		
	}
	
}
