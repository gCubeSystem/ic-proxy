package org.gcube.informationsystem.icproxy.resources;

import static org.gcube.resources.discovery.icclient.ICFactory.client;
import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import lombok.extern.slf4j.Slf4j;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.HostingNode;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;

@Slf4j
@Path("HostingNode")
public class HostingNodeResource {

	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<HostingNode> retrieve() {
		SimpleQuery query = queryFor(HostingNode.class);
		query.addCondition("$resource/Profile/GHNDescription/Status/text() eq \"certified\"");
		DiscoveryClient<HostingNode> client = clientFor(HostingNode.class);
		List<HostingNode> endpoints = client.submit(query);
		log.debug("retrieved resources are "+endpoints.size());
		return endpoints; 
	}

	@GET
	@Path("/{id}/GCoreEnpoints")
	@Produces(MediaType.APPLICATION_XML)
	public List<GCoreEndpoint> retrieveGcoreEnpoints(@NotNull @PathParam("id") String id) {
		log.info("hostingnode called for GCoreEndpoint running on HostingNode with id {}",id);
		SimpleQuery query = queryFor(GCoreEndpoint.class);
		query.addCondition(String.format("$resource/Profile/GHN/@UniqueID/string() eq '%s'",id));
		DiscoveryClient<GCoreEndpoint> client = clientFor(GCoreEndpoint.class);
		List<GCoreEndpoint> endpoints = client.submit(query);
		log.debug("retrieved resources are "+endpoints.size());
		return endpoints; 
	}

	@GET
	@Path("/CustomQuery")
	@Produces(MediaType.TEXT_XML)
	public String retrieve(@NotNull @QueryParam("result") String resultXPath) {
		log.info("hostingnode called (with result {}) ", resultXPath);
		
		SimpleQuery query = queryFor(HostingNode.class);
		
		if (resultXPath!=null && !resultXPath.isEmpty())
			if (resultXPath.startsWith("/"))
				query.setResult("$resource"+resultXPath);
			else
				query.setResult("$resource/"+resultXPath);
				
		query.addCondition("$resource/Profile/GHNDescription/Status/text() eq \"certified\"");
		DiscoveryClient<String> client = client();
		List<String> endpoints = client.submit(query);
		StringBuilder builder = new StringBuilder("<Results>");
		for (String single: endpoints)
			builder.append("<Result>").append(single.replaceAll("\n", "")).append("</Result>");
		builder.append("</Results>");
		log.debug("retrieved resources are "+endpoints.size());
		return builder.toString(); 
	}
	
}
