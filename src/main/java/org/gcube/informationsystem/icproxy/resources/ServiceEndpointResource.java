package org.gcube.informationsystem.icproxy.resources;

import static org.gcube.resources.discovery.icclient.ICFactory.client;
import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.resources.gcore.*;
import org.gcube.common.resources.gcore.utils.Group;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.informationsystem.icproxy.profiles.ServiceEndpointProfile;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.common.encryption.StringEncrypter;

@Slf4j
@Path("ServiceEndpoint")
public class ServiceEndpointResource {
	
	public static final String DECRYPT_ROLE ="service-endpoint-key";
	public static final String CREATE_ROLE ="is-resource-create";
	
	@GET
	@Path("/{category}")
	@Produces(MediaType.APPLICATION_XML)
	public List<ServiceEndpoint> retrieve(@NotNull @PathParam("category") String resourceCategory) {
		log.info("ServiceEndpoint called with category {} in context {}",resourceCategory, ScopeProvider.instance.get());
		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> endpoints = client.submit(getQuery(resourceCategory));
		log.debug("retrieved resources are "+endpoints.size());
		return endpoints; 
	}

	@GET
	@Path("/{category}/{name}")
	@Produces(MediaType.APPLICATION_XML)
	public List<ServiceEndpoint> retrieve(@NotNull @PathParam("name") String resourceName,
										  @NotNull @PathParam("category") String resourceCategory, @QueryParam("decrypt") boolean isDecrypt) {
		log.info("ServiceEndpoint called with category {} and name {} in scope {}",resourceCategory, resourceName, ScopeProvider.instance.get());
		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> endpoints = client.submit(getQuery(resourceName, resourceCategory));
		if(Objects.nonNull(endpoints)) {
			log.debug("retrieved resources are "+endpoints.size());
			return getServiceEndpoints(isDecrypt, endpoints);
		}else{
			log.error("ServiceEndpoint called with category {} and name {} in scope {}, return NULL",resourceCategory, resourceName, ScopeProvider.instance.get());
			return null;
		}
	}

	@GET
	@Path("/{category}/{name}/Result/{result:([^$\\?]+)}")
	@Produces(MediaType.TEXT_XML)
	public String retrieveCustom(@NotNull @PathParam("name") String resourceName, 
			@NotNull @PathParam("category") String resourceCategory, @NotNull @PathParam("result") String resultXPath) {
		log.info("ServiceEndpoint called with category {} and name {} and result {} in scope {}"
				,resourceCategory, resourceName, resultXPath, ScopeProvider.instance.get());
		SimpleQuery query = getQuery(resourceName, resourceCategory);
		if (resultXPath.startsWith("/"))
			query.setResult("$resource"+resultXPath);
		else
			query.setResult("$resource/"+resultXPath);
		DiscoveryClient<String> client = client();
		List<String> endpoints = client.submit(query);
		StringBuilder builder = new StringBuilder("<Results>");
		for (String single: endpoints)
			builder.append("<Result>").append(single.replaceAll("\n", "")).append("</Result>");
		builder.append("</Results>");
		log.debug("retrieved resources are "+endpoints.size());
		return builder.toString(); 
	}

	/**
	 * Used for creating a new ServiceEndpoint resource starting from a XML resource as input param
	 * @param resourceProfile a complete ServiceEndpoint resource in XML
	 * @return the resource id
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_XML)
	public String create(ServiceEndpoint resourceProfile) {
		log.debug("Try to create new serviceEndpoint profile {} in {}",resourceProfile, ScopeProvider.instance.get());
		if (Objects.nonNull(resourceProfile) && isRoleEnabled(CREATE_ROLE)){
			log.debug("going to encrypt and push");
			String id=registerSE(encryptResource(resourceProfile));
			log.debug("pushed on IS");
			return id;
		}else{
			throw new RuntimeException("ServiceEndpoint profile is null or user not enabled");
		}

	}

	/**
	 * Used for creating a new ServiceEndpoint resource starting from a simplified JSON resource
	 * @param resourceProfile a simplified JSON resource defined by ServiceEndpointProfile class
	 * @return the new resource created in XML format
	 */
	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceEndpoint create(ServiceEndpointProfile resourceProfile) {
		log.debug("Try to create new serviceEndpoint profile {} in {}",resourceProfile, ScopeProvider.instance.get());
		if (Objects.nonNull(resourceProfile) && isRoleEnabled(CREATE_ROLE)){
			ServiceEndpoint newResource= new ServiceEndpoint();
			ServiceEndpoint.Profile profile=newResource.newProfile().category(resourceProfile.getCategory()).name(resourceProfile.getName());
		    profile.newPlatform().name(resourceProfile.getPlatform()).version((short)1).minorVersion((short)0).revisionVersion((short)0).buildVersion((short)0);
			profile.newRuntime().hostedOn(resourceProfile.getHost()).status("ready").ghnId("");
			ServiceEndpoint.AccessPoint ap=new ServiceEndpoint.AccessPoint();
			ap.name(resourceProfile.getAccessPointName());
			ap.address(resourceProfile.getAccessPointAddress());
			ap.credentials(resourceProfile.getAccessPointPass(), resourceProfile.getAccessPointUsername());
			profile.accessPoints().add(ap);
			log.debug("going to encrypt and push");
			registerSE(encryptResource(newResource));
			log.debug("pushed on IS");
			return newResource;
		}else{
			throw new RuntimeException("ServiceEndpoint profile is null or user not enabled");
		}

	}

	private SimpleQuery getQuery(String resourceName, String resourceCategory){
		SimpleQuery query = queryFor(ServiceEndpoint.class);
		query.addCondition(String.format("$resource/Profile/Name/text() eq '%s'",resourceName));
		query.addCondition(String.format("$resource/Profile/Category/text() eq '%s'",resourceCategory));
		return query;
	}
	
	private SimpleQuery getQuery(String resourceCategory){
		SimpleQuery query = queryFor(ServiceEndpoint.class);
		query.addCondition(String.format("$resource/Profile/Category/text() eq '%s'",resourceCategory));
		return query;
	}

	private ServiceEndpoint decryptResource(ServiceEndpoint resource) {
		log.debug("encrypting serviceEndpoint {} ", resource.id());
		Group<ServiceEndpoint.AccessPoint> aps=resource.profile().accessPoints();
		for (ServiceEndpoint.AccessPoint ap : aps){
			String decrypted =decryptString(ap.password());
			String user= ap.username();

			ap.credentials(decrypted, user);
		}
		return resource;
	}
	
	private ServiceEndpoint encryptResource(ServiceEndpoint resource) {
		log.debug("encrypting serviceEndpoint {} ", resource.id());
		Group<ServiceEndpoint.AccessPoint> aps=resource.profile().accessPoints();
		for (ServiceEndpoint.AccessPoint ap : aps){
			String decrypted =encryptString(ap.password());
			String user= ap.username();
			ap.credentials(decrypted, user);
		}
		return resource;
	}	
		

	private static String decryptString(String toDecrypt){
		log.debug("decrypting string");
		try{
			return StringEncrypter.getEncrypter().decrypt(toDecrypt);
		}catch(Exception e) {
			throw new RuntimeException("Unable to decrypt : "+toDecrypt,e);
		}
	}
	
	private static String encryptString(String toEncrypt){
		log.debug("encrypting string");
		try{
			return StringEncrypter.getEncrypter().encrypt(toEncrypt);
		}catch(Exception e) {
			throw new RuntimeException("Unable to encrypt : "+toEncrypt,e);
		}
	}
	
	private List<ServiceEndpoint> getServiceEndpoints(boolean isDecrypt, List<ServiceEndpoint> endpoints) {
		if (isDecrypt) {
			if (isRoleEnabled(DECRYPT_ROLE)){
				List<ServiceEndpoint> ses = new ArrayList<>(endpoints.size());
				for (ServiceEndpoint resource : endpoints) {
					ses.add(decryptResource(resource));
				}
				return ses;
			}else{
				log.warn("user not enabled to see the resource free to air, sorry");
			}
		}
		return endpoints;
	}

	private boolean isRoleEnabled(String role){
		boolean isEnabled = false;
		log.debug("checking role: "+role);
		String at= AccessTokenProvider.instance.get();
		try{
			if (ModelUtils.getAccessTokenFrom(at).getRealmAccess().getRoles().contains(role)) {
				isEnabled=true;
			}
		}catch (Exception e){
			log.error("token not retrieved properly: "+e.getMessage());
			e.printStackTrace();
		}
		log.debug("role enabled "+isEnabled);
		return isEnabled;
	}
	private static String registerSE(ServiceEndpoint toRegister) {
		log.trace("going to create a new ServiceEndpoint resource. registerSE method");
		RegistryPublisher rp= RegistryPublisherFactory.create();
		if(log.isDebugEnabled())
			Resources.print(toRegister);
		Resource r=rp.create(toRegister);
		if (Objects.nonNull(r)) {
			log.debug("resource registered with id " + r.id());
			return r.id();
		}else{
			throw new RuntimeException("Creation failed, null returned: "+toRegister);
		}
	}

}