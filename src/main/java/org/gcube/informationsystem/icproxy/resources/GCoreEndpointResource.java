package org.gcube.informationsystem.icproxy.resources;


import static org.gcube.resources.discovery.icclient.ICFactory.client;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;


@Path("GCoreEndpoint")
@Slf4j
public class GCoreEndpointResource {


	@GET
	@Path("/{class}{name:(/[^/?$]+)?}")
	@Produces(MediaType.TEXT_XML)
	public String retrieveCustom(@NotNull @PathParam("class") String serviceClass, 
			@PathParam("name") String serviceName, @QueryParam("result") String resultXPath) {
		try{
			String scope = ScopeProvider.instance.get();
			log.info("gcoreendpoint wuth result called with serviceclass {} and servicename {} and result {} in scope {}",serviceClass, serviceName, resultXPath, scope);
			

			SimpleQuery query = createClassQuery(serviceClass);
			if (serviceName!=null && !serviceName.isEmpty() ){
				serviceName = serviceName.replace("/", "");
				query.addCondition(String.format("$resource/Profile/ServiceName/text() eq '%s'",serviceName));
			}

			if (resultXPath!=null && !resultXPath.isEmpty())
				if (resultXPath.startsWith("/"))
					query.setResult("$resource"+resultXPath);
				else
					query.setResult("$resource/"+resultXPath);

			DiscoveryClient<String> client = client();
			List<String> endpoints = client.submit(query);
			StringBuilder builder = new StringBuilder("<Results>");
			for (String single: endpoints)
				builder.append("<Result>").append(single.replaceAll("\n", "")).append("</Result>");

			builder.append("</Results>");

			log.debug("retrieved resources are "+endpoints.size());
			return builder.toString();
		}catch(Exception e){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("Error : "+e.getMessage()).type(MediaType.TEXT_PLAIN).build()); 
		}
	}

	SimpleQuery createClassQuery(String serviceClass){
		SimpleQuery query = queryFor(GCoreEndpoint.class);
		query.addCondition(String.format("$resource/Profile/ServiceClass/text() eq '%s'",serviceClass));
		query.addCondition("$resource/Profile/DeploymentData/Status/text() eq 'ready'");
		return query;
	}

}
