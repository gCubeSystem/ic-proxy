package org.gcube.informationsystem.icproxy.profiles;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name="resource")
public class ServiceEndpointProfile {
    @Getter
    @Setter
    private String category;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String platform="d4science";
    @Getter
    @Setter
    private String accessPointName;
    @Getter
    @Setter (AccessLevel.PROTECTED)
    private String accessPointAddress;
    @Getter
    @Setter (AccessLevel.PROTECTED)
    private String accessPointUsername;
    @Getter
    @Setter (AccessLevel.PROTECTED)
    private String accessPointPass;
    @Getter
    @Setter (AccessLevel.PROTECTED)
    private String host;
}
